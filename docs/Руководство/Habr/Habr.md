---
sidebar_position: 1
---

# Habr

Жаргон функционального программирования - https://habr.com/ru/post/310172

Объясняем современный JavaScript динозавру - https://habr.com/ru/company/mailru/blog/340922

Шпаргалка для технического собеседования - https://habr.com/ru/company/vk/blog/350326/

Как работают веб-приложения - https://habr.com/ru/post/450282/

Для чего программисту Continuous Integration и с чего начинать - https://habr.com/ru/post/353194/

Стажёр Вася и его истории об идемпотентности API - https://habr.com/ru/company/yandex/blog/442762/

Как научить людей использовать Git - https://habr.com/ru/post/437000/

Лабораторная работа: введение в Docker с нуля. Ваш первый микросервис - https://habr.com/ru/post/346634/

Знакомство с GraphQL на вечеринке - https://habr.com/ru/post/470097/

Ликбез по типизации в языках программирования - https://habr.com/ru/post/161205/

Использование Babel и Webpack для настройки React-проекта с нуля - https://habr.com/ru/company/ruvds/blog/436886/

Как работают кодировки текста. Откуда появляются «кракозябры». Принципы кодирования. Обобщение и детальный разбор - https://habr.com/ru/post/478636/

10 возможностей VS Code, помогающих ускорить работу программиста - https://habr.com/ru/company/ruvds/blog/478320/

Как дата-сайентист машину покупал - https://habr.com/ru/company/ruvds/blog/478328/

Новичку only: 10 вещей, которые вы должны знать как веб-разработчик - https://habr.com/ru/post/478298/

Как работает JS: о внутреннем устройстве V8 и оптимизации кода - https://habr.com/ru/company/ruvds/blog/337460/

Насколько важен порядок свойств в объектах JavaScript? - https://habr.com/ru/post/486162/

Как сделать из сайта приложение и выложить его в Google Play за несколько часов. Часть 1/2: Progressive Web App - https://habr.com/ru/company/vk/blog/450504/

Создаем приложение на JavaScript с помощью React Native - https://habr.com/ru/company/plarium/blog/303328/

Vue.js для начинающих, урок 2: привязка атрибутов - https://habr.com/ru/company/ruvds/blog/509702/

Использование Grid для макетов страниц, а Flexbox — для макетов компонентов - https://habr.com/ru/company/ruvds/blog/506774/

Всё о ключевом слове auto в CSS - https://habr.com/ru/company/ruvds/blog/494716/

Scrum vs Kanban: в чем разница и что выбрать? - https://habr.com/ru/company/hygger/blog/351048/

Словарик айтишника или Что? Где? Куда? Часть 2 - https://habr.com/ru/company/wrike/blog/477936/

Словарик айтишника или Что? Где? Куда? Часть 1 - https://habr.com/ru/company/wrike/blog/475558/

Методы скрытия элементов веб-страниц - https://habr.com/ru/company/ruvds/blog/485640/

REST API Best Practices - https://habr.com/ru/post/351890/

JSON API – работаем по спецификации - https://habr.com/ru/company/oleg-bunin/blog/433322/

Что такое UX/UI дизайн на самом деле? - https://habr.com/ru/post/321312/

Разница между веб-сокетами и Socket.IO - https://habr.com/ru/post/498996/

Dapp. Vue.js + ethers.js - https://habr.com/ru/post/551710/

Что такое блокчейн и зачем он нужен - https://habr.com/ru/company/bitfury/blog/321474/

Nuxt.js: Фреймворк для фреймворка Vue.js - https://habr.com/ru/post/336902/

Vue.js для сомневающихся. Все, что нужно знать - https://habr.com/ru/post/329452/

Ещё раз про семь основных методологий разработки - https://habr.com/ru/company/edison/blog/269789/

Верстка email рассылок от А до Я для чайников - https://habr.com/ru/post/252279/

Разработка документации на VuePress - https://habr.com/ru/post/580894/

Начинающим React-разработчикам: приложение со списком дел (покупок) - https://habr.com/ru/company/otus/blog/531656/

Шпаргалка для технического собеседования - https://habr.com/ru/company/vk/blog/350326/

Vue.js и как его понять - https://habr.com/ru/post/351882/

Хуки жизненного цикла Vue.js - https://www.digitalocean.com/community/tutorials/vuejs-component-lifecycle-ru

REST подход - http://fkn.ktu10.com/?q=node/10262


