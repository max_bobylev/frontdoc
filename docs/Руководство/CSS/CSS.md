---
sidebar_position: 1
---

# Введение в CSS

## Что такое CSS селекторы

```
селектор {
свойство: значение;
свойство: значение;
...
}
```
**Селекторы** — это один из фундаментальных механизмов CSS. Именно они определяют то, к каким элементам будут применены стили, указанные в фигурных скобках.

- Пример CSS правила:

```
p {
text-align: center;
font-size: 20px;
}
```
В этом CSS правиле, <code>p</code> — это селектор, в данном случае — это селектор элемента. Это CSS правило устанавливает стили (CSS свойства, описанные в нём) для всех элементов <code>p</code> на странице.

В CSS очень много различных типов селекторов. Используя один из них или комбинацию из нескольких можно очень точно применить стили к нужным элементам.

## Базовые селекторы
К базовым селекторам можно отнести селектор по классу, тегу, идентификатору, атрибуту и универсальный селектор.

### Селектор по элементу (тегу)
Селектор по элементу предназначен для выбора элементов по имени тега.

**Синтаксис: `имяТега`**

- Пример задания правила для всех элементов <code>p</code> на странице:

```
/* селектор p выберет все элементы p на странице */
p {
padding-bottom: 15px;
}
```
