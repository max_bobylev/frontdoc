import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'CSS, HTML, JS',
    Svg: require('../../static/img/web.svg').default,
    description: (
      <>
          Это три кита, на которых стоит любое frontend-приложение.
      </>
    ),
  },
  {
    title: 'Frameworks',
    Svg: require('../../static/img/framework.svg').default,
    description: (
      <>
          JavaScript Frameworks — это инструменты для построения динамических веб/мобильных/настольных приложений на языке JavaScript. Три основных это <code>React</code> , <code>Vue</code> и <code>Angular</code>.
      </>
    ),
  },
  {
    title: 'UI/UX',
    Svg: require('../../static/img/ui.svg').default,
    description: (
      <>
          <code>UX</code> – это процесс, а <code>UI</code> – это инструмент.  <code>UX</code> шире <code>UI</code> и включает в себя это понятие. Но у них одна цель – сделать взаимодействие пользователя с сайтом удобным, приятным и запоминающимся.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
